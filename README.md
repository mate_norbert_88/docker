#docker-compose
Csináltam egy docker-compose fájlt, amiben összefogtam a dolgokat.

# Angular
Az Angular dev server local 4200-as portja ki van forwardolva, hogy az 1200-as porton elérd a host gépen. \
Sajnos nem igazán jól működő ez a megoldást, mert alapesetben ha módosítasz valamit, akkor ő érzékeli és újrabuildel, 
de így dockerből futva sajnos csak a container újraindításával frissül a kód.

# PHP
Itt a 1080-as porton éred el az apache-ot. \
Az APACHE_DOCUMENT_ROOT be van állítva a Lumen gyökerén belül a public könyvtárra. 

# DB
A hivatalos postgres image futkározik benne az 5432-es porton.